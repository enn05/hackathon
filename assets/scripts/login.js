// add an event listener to the button
loginUser.addEventListener("click", () => {
	// capture the details
	let email = document.querySelector("#email").value;
	let password = document.querySelector("#password").value;



	console.log(email);
	console.log(password);
	// create the new FormData
	let data = new FormData;
	
	// append the new data
	data.append("email", email);
	data.append("password", password);
	
	// use fetch to access our process_authenticate.php
	fetch("../../controllers/process_login.php", {
		method: "POST",
		body: data
	}).then(response => {
			return response.text();
	}).then(data_from_fetch => {
			console.log(data_from_fetch);
			if(data_from_fetch == "login_failed"){
				document.querySelector("#email").nextElementSibling.innerHTML = "Please provide correct credentials";
			} else {
				window.location.replace("../../views/searcher_page.php");
			}
		})
})

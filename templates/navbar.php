<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Destiny</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav ml-auto">
      <?php
        session_start();
        if(isset($_SESSION['user'])){
      ?>
        <li class="nav-item">
          <a class="nav-link" href="#">Hello <?php echo $_SESSION['user']['name'] ?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../controllers/logout.php">Logout</a>
        </li>
      <?php
        } else {
        ?>
        <li class="nav-item active">
          <a class="nav-link" href="../views/login.php">Login</span></a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="../views/register.php">Register</span></a>
        </li>
        <?php
        }
      ?>
    </ul>
  </div>
</nav>
<html>
<head>
	<meta charset="UTF-8">
	<title>Native PHP with MySQL</title></title>
	<link rel="stylesheet" href="https://bootswatch.com/4/journal/bootstrap.css">
	<link rel="stylesheet" href="../assets/styles/style.css">
</head>
<body>
	<?php require 'navbar.php' ?>
	<?php get_content() ?>
	<?php require 'footer.php' ?>
</body>
</html>
<?php 
	

	require '../templates/template.php';
	function get_content(){
		require '../controllers/connection.php';
			?>
			
<div class="container">
	<h1 class="text-center py-5">Welcome</h1>
	<div class="row">

		<?php
		$users_query = "SELECT * FROM users";
		$users = mysqli_query($conn,$users_query);
		//review this later
		
		foreach ($users as $user){
		 //if(isset($_SESSION['user'])&& $_SESSION['user']['role_id']==1){
		?>
			<div class="col-lg-3 py-2">
				<div class="card">
					<img src="<?php echo $user['image'] ?>" alt="image" height="200px">
					<h3 class="text-info"><?php echo $user['name'] ?></h3>
					<p class="text-dark"><?php echo $user['sex'] ?></p>
					<p class="text-center"><a href="edit.php?id=<?php echo $user['id']?>">Edit</a> <a href="../controllers/process_delete.php?id=<?php echo $user['id']?>">Delete</a></p>
					
				</div>
			</div>

		<?php
		}

	?>

	</div>
</div>
	<?php

	}
 ?>

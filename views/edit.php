<?php 
	

	require '../templates/template.php';
	function get_content(){
		require '../controllers/connection.php';
			?>
			
<div class="container vh-100">
	<h1 class="text-center py-5">Edit Profiles</h1>
	<div class="row">

		<?php
		$id = $_GET['id'];
		$users_query = "SELECT * FROM users where id =$id";
		$user = mysqli_fetch_assoc(mysqli_query($conn,$users_query));
		// var_dump($user);
		// die();
		

		?>

			<div class="form">
					
				<form action="../controllers/process_edit.php" method="POST" enctype="multipart/form-data">
					<div class="form-group">
						<label for="name"></label>
						<input type="text" name="name" value="<?php echo $user['name']  ?>">
						<input type="hidden" value="<?php echo $user['id'] ?>" name="id">		
					</div>
					<div class="form-group">
						<label for="sex">Gender</label>
						<select name="sex" id="">
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
							
					</div>
					<div class="form-group">
						<label for="image">Image</label><br/>
						<span class=""> Previous image</span><img src="<?php echo $user['image'] ?>" alt="image" height="50px">
						<input type="file" name="image" value="<?php echo $user['image']  ?>">		
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">Enter Changes</button>
					</div>
				</form>

			</div>

			

		<?php
		

	?>

	</div>
</div>
	<?php

	}
 ?>

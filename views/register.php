<?php 
	require "../templates/template.php";

	function get_content(){
		// require "../controllers/connection.php";
?>
		<h1 class="text-center py-5">Register</h1>
		<div class="col-lg-8 offset-lg-2">
			<form action="../controllers/process_register.php" method="POST">
				<div class="form-group">
					<label for="name">Full Name</label>
					<input type="text" name="name" class="form-control" id="name">
					<span class="validation"></span>
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" name="email" class="form-control" id="email">
					<span class="validation"></span>
				</div>
				<div class="form-group">
					<label for="image">Image:</label>
					<input type="file" name="image" class="form-control">
				</div>
				<div class="form-group">
					<label for="category_id">Gender</label>
					<select name="sex" class="form-control">
						<option value="0">Female</option>
						<option value="1">Male</option>
					</select>
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" class="form-control" id="password">
					<span class="validation"></span>
				</div>
				<!-- <div class="form-group">
					<label for="confirm">Confirm Password</label>
					<input type="password" name="confirm" class="form-control" id="confirm">
					<span class="validation"></span>
				</div> -->
				<button type="submit" class="btn btn-warning" id="registerUser">Register</button>
			</form>
			<p>Already Registered? <a href="login.php">Login</a></p>
		</div>
		<!-- <script src="../assets/scripts/register.js"></script> -->
<?php
	}

 ?>

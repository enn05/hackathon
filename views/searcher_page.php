<?php 
	require "../templates/template.php";
	// require "../controllers/connection.php";

	function get_content(){
	// session_start();
	require "../controllers/connection.php";



	if(isset($_SESSION['user'])){
?>
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6 py-2">
					<div class="card">
						<img class="card-img-top" height="300px" src="<?php echo $_SESSION['user']['image'] ?>" alt="">
						<div class="card-body">
							<h4 class="card-title text-center"><?php echo $_SESSION['user']['name'] ?></h4>
						</div>
						<div class="card-footer text-center">
							<p>Search you match</p>
							<a href="<?php  ?>" class="btn btn-danger">Male</a>
							<a href="#" class="btn btn-info">Female</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 py2">
					<div class="row">
						<?php
							$user_entry_query = "SELECT * FROM `users` ORDER BY RAND() LIMIT 3";

							$users = mysqli_query($conn, $user_entry_query);
							// var_dump($users);
							// die();
							foreach($users as $indiv_user){
						?>
							<div class="col-lg-4">
								<div class="card">
									<img class="card-img-top" height="300px" src="<?php echo $indiv_user['image'] ?>" alt="">
								</div>
							</div>
						<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>
<?php
	}
?>
<?php
	}
?>